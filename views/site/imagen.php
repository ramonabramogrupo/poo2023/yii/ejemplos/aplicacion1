<?php

use yii\helpers\Html;

?>
<h2>
    Imagen
</h2>

<?= Html::img("@web/imgs/{$foto}", [ // ruta de la imagen 
    'alt' => 'ordenador', // atributos de la etiqueta img
    'class' => 'rounded img-thumbnail col-sm-2 ',
]) ?>